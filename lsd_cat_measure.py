#!/usr/bin/env python
##
# FILE: lsd_cat_measure.py
# AUTHOR: E. C. Herenz

# lsdcat libraries 
import lib.lsd_cat_lib as lsd_cat_lib
import lib.lsd_cat_measure_lib as lsd_cat_measure_lib
from lib.line_em_funcs import get_timestring
from lib.line_em_funcs import read_hdu
from lib.line_em_funcs import int_or_str
from lib.cats import cats


__version__ = lsd_cat_lib.get_version()

# CONSTANTS THAT ARE RELEVANT FOR THE CATALOG ENTRIES, BUT THEY SHOULD
# BE CHANGED BY THE USER ONLY AT OWN RISK
ws_fixed = 25.  # half size of the subcubes on which subcubes are
                # extracted around the objects for analysis - i.e. 25
                # results in 50x50x50 cubes - seems to be enough... if
                # a galaxy is bigger than 1/4 of the FOV, than you
                # don't wanna measure fluxes with this tool, anyway
z_diff_max = 25 # maximum size of narrow band window ... this value should be OK
flux_unit_str = '10**(-20)*erg/s/cm**2'  # could be automatically created...
times_fwhm_const = 2.5  # multiplicator to define width of the window
                        # in which weighted positional parameters are determined
muse_spaxscale = 0.2  # '' - probably better to parse from header in
                      # the future - see
                      # http://docs.astropy.org/en/stable/api/astropy.wcs.utils.proj_plane_pixel_scales.html#astropy.wcs.utils.proj_plane_pixel_scales

# python standard library
import sys,os,time,datetime
import gc 
import argparse
import string
import random
import math

# required 3rd party libraries (NumPy, SciPy, and Astropy)
import numpy as np
from astropy.io import fits
from astropy.io.fits import Column
from scipy.ndimage import measurements

# get string of the commandline that was entered by the user
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command+' '+entity

# get starting time
starttime = time.time()
    
# command line parsing
class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter,
                      argparse.RawDescriptionHelpFormatter):
        pass

parser = argparse.ArgumentParser(description="""
lsd_cat_measure.py - Measurement of emission lines detected with lsd_cat.py
""",
                                 epilog="""
Supported values for --tabvalues option (columns will be appended to input table):

X_SN,Y_SN,Z_SN - 3D S/N weighted centroid coordinates in voxel coordinates (0-indexed)
RA_SN, DEC_SN, LAMBDA_SN - 3D S/N weighted centroid in the physical woorld coordinate systeem (hereafter physical coordinates).
X_FLUX, Y_FLUX, Z_FLUX - 3D flux weighted centroid in voxel coordinates (0-indexed).
RA_FLUX, DEC_FLUX, LAMBDA_FLUX - 3D flux weighted centroid in physical coordinates.
X_SFLUX, Y_SFLUX, Z_SFLUX - 3D filtered flux weighted centroid in voxel coordinates (0-indexed).
RA_SFLUX, DEC_SFLUX, LAMBDA_SFLUX - 3D filtered flux weighted centroid in physical coordinates.
X_1MOM,Y_1MOM - 1st central moments in flux filtered narrow band (pixel coordinates).
RA_1MOM,DEC_1MOM - 1st central moment in flux filtered narrow band (physical coordinates).
Z_NB_MIN,Z_NB_MAX - Narrowband boundary coordinates (layer coordinates).
LAMBDA_NB_MAX,LAMBDA_NB_MIN - Narrowband boundary coordinates (wavelength - see note below!).

X_2MOM,Y_2MOM,XY_2MOM - Second central moments in flux filtered narrow band.
SIGMA_ISO - Square-root of (X_2MOM + Y_2MOM)/2.
F_KRON,F_2KRON,F_3KRON,F_4KRON - Flux in 1, 2, 3, or 4 Kron-Radii in between Z_NB_MIN and Z_NB_MAX.
F_KRON_ERR,F_2KRON_ERR,F_3KRON_ERR,F_4KRON_ERR - Propagated error on flux measurement.
Z_DIFF_FLAG - Maximum- / Minimum narrow band window used.
RKRON - Kron Radius.
RRON_FLAG - Minimum radius reached in RKRON calculation.

Note: Z_NB_MIN and Z_NB_MAX refers to the layer center coordinates, while
LAMBDA_NB_MIN and LAMBDA_NB_MAX refer to the layer boundary coordinates.

""",
                                 formatter_class=CustomFormatter)  # TODO: add descriptions

out_line_form = {'X_SN':            ('%10.2f','x_sn_com','','D'),
                 'Y_SN':            ('%10.2f','y_sn_com','','D'),
                 'Z_SN':            ('%10.2f','z_sn_com','','D'),
                 'RA_SN':           ('%12.6f','ra_sn_com','deg','D'),
                 'DEC_SN':          ('%12.6f','dec_sn_com','deg','D'),
                 'LAMBDA_SN':       ('%10.2f','lambda_sn','Angstrom','D'),
                 'X_FLUX':          ('%10.2f','x_flux_com','','D'),
                 'Y_FLUX':          ('%10.2f','y_flux_com','','D'),
                 'Z_FLUX':          ('%10.2f','z_flux_com','','D'),
                 'RA_FLUX':         ('%12.6f','ra_flux_com','deg','D'),
                 'DEC_FLUX':        ('%12.6f','dec_flux_com','deg','D'),
                 'LAMBDA_FLUX':     ('%10.2f','lambda_flux_com','Angstrom','D'),                 
                 'X_SFLUX':          ('%10.2f','x_sflux_com','','D'),
                 'Y_SFLUX':          ('%10.2f','y_sflux_com','','D'),
                 'Z_SFLUX':          ('%10.2f','z_sflux_com','','D'),
                 'RA_SFLUX':         ('%12.6f','ra_sflux_com','deg','D'),
                 'DEC_SFLUX':        ('%12.6f','dec_sflux_com','deg','D'),
                 'LAMBDA_SFLUX':     ('%10.2f','lambda_sflux_com','Angstrom','D'),                 
                 'RKRON': ('%10.2f','r_krons','','D'),
                 'SIGMA_ISO': ('%10.2f','sigma_isos','','D'),
                 'X_1MOM': ('%10.2f','x_1moms','','D'),
                 'Y_1MOM': ('%10.2f','y_1moms','','D'),
                 'RA_1MOM': ('%12.6f','ra_1moms','deg','D'),
                 'DEC_1MOM': ('%12.6f','dec_1moms','deg','D'),
                 'Z_NB_MIN':  ('%10.2f','z_mins','','D'),
                 'LAMBDA_NB_MIN': ('%10.2f','lambda_mins','Angstrom','D'),
                 'Z_NB_MAX':  ('%10.2f','z_maxs','','D'),
                 'LAMBDA_NB_MAX': ('%10.2f','lambda_maxs','Angstrom','D'),
                 'X_2MOM': ('%10.2f','x_2moms','','D'),
                 'Y_2MOM': ('%10.2f','y_2moms','','D'),                 
                 'XY_2MOM': ('%10.2f','xy_2moms','','D'),
                 # 'A': ('%10.2f','aas','','D'),
                 # 'B': ('%10.2f','bs','','D'),
                 # 'THETA': ('%10.2f','thetas','deg','D'),
                 'F_KRON': ('%10.2f','f_krons',flux_unit_str,'D'),
                 'F_2KRON': ('%10.2f','f_2krons',flux_unit_str,'D'),
                 'F_3KRON': ('%10.2f','f_3krons',flux_unit_str,'D'),
                 'F_4KRON': ('%10.2f','f_4krons',flux_unit_str,'D'),                 
                 'F_KRON_ERR': ('%10.2f','f_krons_err',flux_unit_str,'D'),
                 'F_2KRON_ERR': ('%10.2f','f_2krons_err',flux_unit_str,'D'),
                 'F_3KRON_ERR': ('%10.2f','f_3krons_err',flux_unit_str,'D'),
                 'F_4KRON_ERR': ('%10.2f','f_4krons_err',flux_unit_str,'D'),
                 'Z_DIFF_FLAG': ('%6d','z_diff_flag','','L'),
                 'RKRON_FLAG': ('%6d','r_kron_flag','','L')
                 }

# A
# B
# THETA


# input catalog 
parser.add_argument('-ic','--inputcat',required=True,type=str,
help="""Input LSDCat catalogue from lsd_cat.py (FITS file). This catalogue must contain
the fields I,X_PEAK,Y_PEAK,Z_PEAK.""")
# FUTURE: We try to find the datacube files that where used to create this catalog automatically.
# If this does not work, you can set them manually (see below).
parser.add_argument('-ta','--threshana',
                    type=float, required=True,
                    help="""
Analysis threshold.
""")
parser.add_argument('--tabvalues',
                    type=str,
                    default='X_SN,Y_SN,Z_SN,RA_SN,DEC_SN,LAMBDA_SN,'+\
                    'X_FLUX,Y_FLUX,Z_FLUX,RA_FLUX,DEC_FLUX,LAMBDA_FLUX,'+\
                    'X_SFLUX,Y_SFLUX,Z_SFLUX,RA_SFLUX,DEC_SFLUX,LAMBDA_SFLUX,'+\
                    'X_1MOM,Y_1MOM,RA_1MOM,DEC_1MOM,Z_NB_MIN,Z_NB_MAX,Z_DIFF_FLAG,'+\
                    'LAMBDA_NB_MIN,LAMBDA_NB_MAX,X_2MOM,Y_2MOM,XY_2MOM,'+\
                    'RKRON,RKRON_FLAG,SIGMA_ISO,F_KRON,F_KRON_ERR,'+\
                    'F_2KRON,F_2KRON_ERR,F_3KRON,F_3KRON_ERR,F_4KRON,F_4KRON_ERR',
                    help="""
Comma-separated list of values to be written in output catalog. (Default: all).
""")

parser.add_argument('-f','--fluxcube',required=True,
                    type=str,
                    help="""
FITS file containing the flux (+ variance) datacubes. It is recommended to use a
continuum subtracted cube (e.g. median-filter subtracted).
""")
parser.add_argument('--fhdu',default='MEDFILTER-SUBTRACTED_DATA',
                    type=int_or_str,
                    help="""
HDU name (or number) in FLUXCUB` containing the
(continuum subtracted) flux. [Default: MEDFILTER-SUBTRACTED_DATA}]
""")
parser.add_argument('--ferrhdu',default='STAT',help="""
HDU name (or number) in FLUXCUBE containing the variances.
[Default: STAT]
""")
parser.add_argument('-ff','--filteredfluxcube',required=True,
                    type=str,
                    help="""
FITS file containing the datacube and propagated variances after
matched filtering (i.e. after running lsd_cc_spatial.py and
lsd_cc_spectral.py on FLUXCUBE.  
""")
parser.add_argument('--ffhdu',default='FILTERED_DATA',
                    type=int_or_str,
                    help="""
HDU name (or number) in FILTEREDFLUXCUBE that
contains the filtered flux data. [Default: FILTERED_DATA]
""")
parser.add_argument('--fferhdu',default='FILTERED_STAT',
                    type=int_or_str,
                    help="""
HDU name (or number) in `FILTEREDFLUXCUBE` that contains the
propagated variances after matched filtering. [Default: FILTERED_STAT]
""")
parser.add_argument('-ffsn','--sncube',
                    type=int_or_str,
                    default='none',
                    help="""
FITS file containing the S/N cube. (This
cube can be generated with ./tools/s2n-cube.py from
FILTEREDFLUXCUBE.  If not supplied it will be generated
on-the-fly, however generating it in advance saves computing time in
multiple runs.) [Default: None.]
""")
parser.add_argument('--ffsnhdu',default='SIGNALTONOISE',
                    type=int_or_str,
                    help="""
HDU name (or number) in SNCUBE that contains the S/N
data. [Default: SIGNALTONOISE].
""")
parser.add_argument('-t','--thresh', type=float, default=None, required=False,
                    help="""
The detection threshold that was used in the
lsd_cat.py run.  Normally this value is not required, as it will
be read from the FITS header in INPUTCAT.  However, older versions
of lsd_cat.py did not write this header, hence this option ensures
compatibility with those
versions. [Default: Use value in FITS header of catalogue.]
""")
parser.add_argument('--rmin', type=float, default=3.,
                    help="""
Minimum radius for flux extraction aperture (in units of isophotal
radii). [Default: 3]
""")
parser.add_argument('--rmax', type=float, default=6., help="""
Maximum radius for flux extraction aperture (in units of
isophotal radii).  This radius also defines the area that is used to
calculate the Kron-radius. [Default: 6]
""")
parser.add_argument('--ccparams',type=str, default='none',
                    help="""Parameters used in the cross-correlation process with
                    lsd_cc_spatial.py (polynomial coefficents p0,p1,p2
                    describing the PSF FWHM wavelength dependence) and
                    lsd_cc_spectral.py (v_FWHM describing the width of
                    the spectral template in velocity space).  These
                    values are used to define reasonable borders of
                    minicubes around the emission line postions on
                    which the computations are performed. By default
                    these parameters are read from the header of
                    intermediate catalogue produced by lsd_cat.py.
                    However, if the relevant keywords are not found in
                    the header (e.g. if the intermediate catalogue was
                    not created with lsd_cat.py), these values can be
                    supplied here. In this case we use here
                    --ccparams=p0,p1,p2,vfwhm,lambda0 (default:
                    --ccparams=0.8,0.,0.,150.,7050.).  For the description of
                    the paramters see also the documentation of
                    lsd_cc_spatial.py and lsd_cc_spectral.py. """)
parser.add_argument("-c","--catalog",type=str,default=None,
                    help="""
Filename of the FITS table containing the
output catalogue with the columns as defined in
TABVALUES. [Default: INPUTCAT+`_fluxes.fits`]
""")
parser.add_argument("--clobber",action='store_true',help="""
Overwrite already existing output file! 
USE WITH CAUTION AS THIS MAY OVERWRITE YOUR RESULTS!
""")

# parser.add_argument('--kscale', type=float, default=2.5, help="""
# Scale factor for KRON aperture used in 'FLUX_KKRON' extraction. (Default: 2.5).
# """)

args = parser.parse_args()

table_filename = args.inputcat
thresh = args.thresh
thresh_ana = args.threshana
R_min = args.rmin
R_max = args.rmax

if args.catalog == None:
    assert table_filename[-5:] == '.fits'
    outfitstable_name = table_filename[:-5]+'_fluxes.fits'
else:
    outfitstable_name = args.catalog
    assert outfitstable_name[-5:] == '.fits'

if args.clobber != True and os.path.isfile(outfitstable_name):
    print(outfitstable_name+' already exists. Use --clobber to overwrite! ABORTING NOW!')
    sys.exit(2)
    

tabvalues = args.tabvalues.split(',')
# test if all entries in --tabvalues are recognised
lsd_cat_lib.tabvalue_test(tabvalues,out_line_form)


print(' ... LSDCat Measurement Routine Version '+str(__version__)+' ... ')
print(cats[3])

# OPEN CATALOG
table = fits.getdata(table_filename, 1)  # implicitly assuming table is stored in HDU 1
table_header = fits.getheader(table_filename,1)
try:
    running_ids = table['I']
    # ids = table['ID']
    x_peak_sns = table['X_PEAK_SN']
    y_peak_sns = table['Y_PEAK_SN']
    z_peak_sns = table['Z_PEAK_SN']
except:
    print('ERROR: provided input catalog '+table_filename+\
          ' does not contain the required input fields: '+\
          'I,X_PEAK_SN,Y_PEAK_SN,Z_PEAK_SN')
    sys.exit(2)

# reading detection threshold from catalogue
try:
    thresh = table_header['THRESH']
except KeyError:
    if thresh == None:
        print('NO DETECTION THRESHOLD PROVIDED (--thresh) AND NONE FOUND IN HEADER OF INPUT CATALOGUE! ABORTING NOW!')
        sys.exit(2)
    else:
        assert thresh_ana <= thresh
        pass

if args.sncube == 'none':
    print(table_filename+': No --sncube given. Opening filtered flux cube ... ('+\
          args.filteredfluxcube+').')
    print('... Reading in filtered flux HDU '+str(args.ffhdu)+'. '+\
          get_timestring(starttime))
    # open filtered flux cube
    flux_filt_cube, flux_filt_header = read_hdu(args.filteredfluxcube,
                                                args.ffhdu,
                                                nans_to_value=True,nanvalue=0.)
    print(table_filename+': Reading in propgated variance HDU '+str(args.fferhdu)+'. '+\
          get_timestring(starttime))
    flux_filt_err_cube, flux_filt_err_cube_header = read_hdu(args.filteredfluxcube,
                                                             args.fferhdu,
                                                             nans_to_value=True,
                                                             nanvalue=float("inf"))
    print(table_filename+': Generating SN cube ... '+get_timestring(starttime))
    sn_cube = flux_filt_cube / np.sqrt(flux_filt_err_cube)

    del flux_filt_err_cube
    del flux_filt_err_cube_header
else:
    print(table_filename+': Opening filtered flux cube ... ('+\
          args.filteredfluxcube+').')
    print(' ... Reading in filtered flux HDU '+str(args.ffhdu)+'. '+\
          get_timestring(starttime))
    flux_filt_cube, flux_filt_header = read_hdu(args.filteredfluxcube,
                                                args.ffhdu,
                                                nans_to_value=True,
                                                nanvalue=0.)
    print(table_filename+': Opening SN cube '+args.sncube+\
          ' (HDU '+str(args.ffsnhdu)+') ... '+get_timestring(starttime))
    sn_cube, sn_cube_header = read_hdu(args.sncube, args.ffsnhdu,
                                       nans_to_value=True, nanvalue=0.)

try:
    cdelt = flux_filt_header['CD3_3']  # wavelength increment
    cdelt_key = 'CD3_3'
except KeyError:
    try: 
        cdelt = flux_filt_header['CDELT3']
        cdelt_key = 'CDELT3'
    except KeyError:
        print('ERROR: No wavelength increment keyword found in header of filtered flux cube!')
        sys.exit(2)

try:
    wavel_unit = flux_filt_header['CUNIT3']
    if wavel_unit != 'Angstrom':
        print('ERROR: Wavelength increment (CUNIT3) not linear in Angstrom, aborting now!')
        sys.exit(2)
except KeyError:
    print('ERROR: No wavelength increment found in header of filtered flux cube (CUNIT3)! Aborting now!')
    sys.exit(2)
    
wavel = lsd_cat_lib.wavel(flux_filt_header, cdel_key=cdelt_key)        

# try to find cross-correlation parameters, either from header of
# input catalogue or from flux_filt_header or sn_cube_header
# or use the one that are supplied by the user
ccparams = args.ccparams
try:
    # this needs to be rewritten... it likely breaks
    ccs_p0 = flux_filt_header['CCSPLYP0']
    ccs_p1 = flux_filt_header['CCSPLYP1']
    ccs_p2 = flux_filt_header['CCSPLYP2']
    ccl_l0 = flux_filt_header['CCSPLYL0']
    ccl_vfwhm = flux_filt_header['CCLVFWHM']
    print(table_filename+': Found cross-correlation parameters in header of filtered fluxcube: ')
    print(table_filename+': p0='+str(ccs_p0)+' arcsec, p1='+str(ccs_p1)+' arcsec/Angstrom,'+\
          ' p2='+str(ccs_p2)+' arcsec/Angstrom**2, lambda0='+str(ccl_l0)+' Angstrom,'+\
          ' v_FWHM='+str(ccl_vfwhm)+' km/s')
    no_cc_header = False
except KeyError:
    print(args.filteredfluxcube+': Required FITS header keywords from matched filtering '+\
          '(CCSPLYP0,CCSPLYP1,CCSPLYP2,CCSPLYL0,CCLVFWHM) not found. ')
    no_cc_header = True

if ccparams != 'none':
    try:
        ccs_p0, ccs_p1, ccs_p2, ccl_vfwhm, ccl_l0 = ccparams.split(',')
        ccs_p0 = float(ccs_p0)
        ccs_p1 = float(ccs_p1)
        ccs_p2 = float(ccs_p2)
        ccl_l0 = float(ccl_l0)
        ccl_vfwhm = float(ccl_vfwhm)
    except ValueError:
        print('ERROR: --ccparams='+ccparams+' is not a valid value for this option.')
        print('IT MUSE BE OF THE FORM --ccparams=p0,p1,p2,lambda0,v_fwhm  -- see ')
        print('DOCUMENTATION OF lsd_cc_spatial.py AND lsd_cc_spectral.py for documentation ')
        print('OF THOSE PARAMETERS! ABORTING NOW!!!')
        sys.exit(2)

    if no_cc_header:    
        print(table_filename+': --ccparams argumen used, using the following paramters: ')
    else:
        print(table_filename+': --ccparams argumen used, overiding the values found in the header'+\
              ' with the following paramters: ')
    print(table_filename+': p0='+str(ccs_p0)+' arcsec, p1='+str(ccs_p1)+' arcsec/Angstrom,'+\
          ' p2='+str(ccs_p2)+' arcsec/Angstrom**2, lambda0='+str(ccl_l0)+' Angstrom,'+\
          ' v_FWHM='+str(ccl_vfwhm)+' km/s')

elif ccparams == 'none' and no_cc_header:
    print(table_filename+': No header keyowrds where found - and --ccparams option not used...')
    print(table_filename+': ... struggling on with default values: --ccparams=0.8,0.,0.150.,7050.')
    ccs_p0, ccs_p1, ccs_p2, ccl_l0, ccl_vfwhm = 0.8, 0., 0., 7050., 150.
    

### ACTUAL MEASUREMENTS START HERE ###
x_min, x_max, y_min, y_max, z_min, z_max = \
        lsd_cat_lib.calc_borders_from_filter(x_peak_sns, y_peak_sns, z_peak_sns,
                                             sn_cube,
                                             wavel, wavel_unit,
                                             ccs_p0,ccs_p1,ccs_p2,ccl_l0,
                                             ccl_vfwhm,
                                             times_fwhm=times_fwhm_const,
                                             spaxel_scale=muse_spaxscale)



# S/N weighted barycenter
sn_weighted_list = ['X_SN','Y_SN','Z_SN',
                    'LAMBDA_SN','RA_SN','DEC_SN']
if any([True
        for element in sn_weighted_list
        if element in tabvalues]):
    print(table_filename+': Calculating (SN weigthed) barycenter of '+\
              'detections ... '+get_timestring(starttime))
    x_sn_com, y_sn_com, z_sn_com = \
            lsd_cat_lib.calc_weighted(x_min, x_max,
                                      y_min, y_max,
                                      z_min, z_max,
                                      sn_cube, thresh_ana=thresh_ana)
    lambda_sn = lsd_cat_lib.calc_lambda(z_sn_com,
                                        flux_filt_header)

    ra_sn_com, dec_sn_com = lsd_cat_lib.pix_to_radec(x_sn_com,
                                                     y_sn_com,
                                                     flux_filt_header)

# filtered flux weighted barycenter
fs_weighted_list = ['X_SFLUX','Y_SFLUX','Z_SFLUX',
                    'RA_SFLUX','DEC_SFLUX','LAMBDA_SFLUX']
if any([True for element in fs_weighted_list
        if element in tabvalues]):
    print(table_filename+': Calculating (filtered flux weigthed) barycenter '+\
          'of detections ... '+get_timestring(starttime))
    x_sflux_com, y_sflux_com, z_sflux_com = \
                        lsd_cat_lib.calc_weighted(x_min, x_max,
                                                  y_min, y_max,
                                                  z_min, z_max,
                                                  sn_cube, thresh_ana=thresh_ana,
                                                  weigh_cube=flux_filt_cube)
    lambda_sflux_com = lsd_cat_lib.calc_lambda(z_sflux_com,
                                              flux_filt_header)
    ra_sflux_com, dec_sflux_com = lsd_cat_lib.pix_to_radec(
        x_sflux_com, y_sflux_com, flux_filt_header)
    

        
# z_min, z_max above analysis threshold - defines NB window for flux extraction
print(table_filename+': Calculating NB windows for moment based '+\
      'parametrisation of sources ... '+get_timestring(starttime))
z_mins, z_maxs = lsd_cat_measure_lib.nb_zmin_zmax(sn_cube,
                                                  x_peak_sns, y_peak_sns, z_peak_sns,
                                                  thresh_ana,ws=ws_fixed)

# if the window gets to broad we fix it to (20 per default, in MUSE
# thats >20A) spectral layers max
z_mins, z_maxs, z_diff_flag = lsd_cat_measure_lib.z_diff_calc(z_mins, z_maxs, z_peak_sns,
                                                              z_diff_max=z_diff_max)

# NB window in wavelength coordinates, but measuring the border of the wavelength bin,
# not the center as in pixel coordinates
lambda_mins = lsd_cat_lib.calc_lambda(z_mins, flux_filt_header) - cdelt / 2.
lambda_maxs = lsd_cat_lib.calc_lambda(z_maxs, flux_filt_header) + cdelt / 2.

# logical masks at peak layers defining the isophotes for calculation
# of sigma_isos
print(table_filename+': Calculating isophotoes at analysis threshold ...'+\
      get_timestring(starttime))
logical_masks_at_peak = \
    lsd_cat_measure_lib.ana_thresh_region_at_peak(sn_cube,
                                                  x_peak_sns,y_peak_sns,z_peak_sns,
                                                  thresh_ana)

# summation from z_min -> z_max + 1 (to include last layer) over filtered MFS cube
print(table_filename+': Creating NB images of filtered fluxcube ...'+\
      get_timestring(starttime))
nb_images = lsd_cat_measure_lib.gen_nb_images(flux_filt_cube, z_mins, z_maxs + 1)

# select regions relevant for image moments
print(table_filename+': Select source pixels that are relevant'+\
      ' for image moments ...'+get_timestring(starttime))
masked_flux_filt_nb_images = \
    lsd_cat_measure_lib.gen_masked_flux_filt_nb_images(logical_masks_at_peak,
                                                       nb_images)

# image moments from those regions
print(table_filename+': Calculation of image moments for the sources ...'+\
      get_timestring(starttime))
x_1moms, y_1moms, x_2moms, y_2moms, xy_2moms = \
                    lsd_cat_measure_lib.sn_2dmoms(masked_flux_filt_nb_images)
# Conversion of x_1moms & y_1moms to right ascension and declination
ra_1moms, dec_1moms = lsd_cat_lib.pix_to_radec(x_1moms, y_1moms, flux_filt_header)

print(table_filename+': Calculation of parameters derived from image moments '+\
      '(e.g. ellipse parameters) ...'+get_timestring(starttime))

# sigma_iso_circ = std. dev assuming circular symmetric Gaussian dist
sigma_isos = lsd_cat_lib.sigma_iso_circ(x_2moms, y_2moms)

# ellipse parameters - FIX: DOES NOT WORK 
# aas, bs, thetas, elongations, ellipticitys = \
#                     lsd_cat_lib.ellipse_parameters(x_2moms, y_2moms, xy_2moms)

# Create the arrays from which the kron radius will be caluclated.
# These arrays are created by selecting a region R_max * sigma_iso
# centered on x_1mom, x_2mom from each masked_flux_filt_nb_image
print(table_filename+': Select pixels that are relevant'+\
      ' for Kron radius calculation ...'+get_timestring(starttime))
kron_calc_images = \
        lsd_cat_measure_lib.create_kron_calc_images(masked_flux_filt_nb_images,
                                                    x_1moms, y_1moms,
                                                    sigma_isos, R_max)

# calculation of Kron radii
print(table_filename+': Calculation of Kron radii ...'+\
      get_timestring(starttime))
r_krons, r_kron_flag = lsd_cat_measure_lib.calc_rkrons(x_1moms, y_1moms,
                                                       kron_calc_images, R_min=R_min)


# open flux cube if flux values are demanded
flux_values_list = ['F_KRON','F_2KRON','F_3KRON',
                    'X_FLUX','Y_FLUX','Z_FLUX','RA_FLUX','DEC_FLUX','LAMBDA_FLUX']
#flux_err_values_list = ['F_KRON_ERR','F_2KRON_ERR','F_3KRON_ERR']
flux_values_demanded = any([True for element in flux_values_list
                            if element in tabvalues])
# flux_err_values_demanded = any([True for element in flux_err_values_list
#                             if element in tabvalues])
if flux_values_demanded:
    print(table_filename+': Flux values demanded - openining flux-cube '+args.fluxcube+\
          '(FluxHDU: '+str(args.fhdu)+') '+get_timestring(starttime))
    del flux_filt_cube  # freeing some memory 
    #del sn_cube
    mfs_cube, mfs_cube_header = read_hdu(args.fluxcube, args.fhdu,
                                         nans_to_value=True,nanvalue=0)
    

    print(table_filename+': Flux variances loading from '+args.fluxcube+\
          ' (FluxHDU: '+str(args.ferrhdu)+')')
    mfs_cube_err, mfs_cube_err_header = read_hdu(args.fluxcube, args.ferrhdu,
                                                 nans_to_value=True,nanvalue=0)


    # flux weighted barycenter
    flux_weighted_list = ['X_FLUX','Y_FLUX','Z_FLUX',
                          'RA_FLUX','DEC_FLUX','LAMBDA_FLUX']
    if any([True for element in flux_weighted_list
            if element in tabvalues]):
        print(table_filename+': Calculating (flux weigthed) barycenter '+\
              'of detections ... '+get_timestring(starttime))
        x_flux_com, y_flux_com, z_flux_com = \
                            lsd_cat_lib.calc_weighted(x_min, x_max,
                                                      y_min, y_max,
                                                      z_min, z_max,
                                                      sn_cube, thresh_ana=thresh_ana,
                                                      weigh_cube=mfs_cube)
        lambda_flux_com = lsd_cat_lib.calc_lambda(z_flux_com,
                                                  mfs_cube_header)
        ra_flux_com, dec_flux_com = lsd_cat_lib.pix_to_radec(
            x_flux_com, y_flux_com, mfs_cube_header)



    print(table_filename+': Flux + error measurment in Kron appertures... ')
    if any([True for element in ['F_KRON','F_KRON_ERR'] if element in tabvalues]):
        print(table_filename+': k_scale = 1 ... '+get_timestring(starttime))

        f_krons, f_krons_err = \
            lsd_cat_measure_lib.fluxes_rkron_circ(mfs_cube,
                                                  x_1moms, y_1moms,
                                                  z_mins, z_maxs, r_krons,
                                                  k_scale=1.0,
                                                  err_cube=mfs_cube_err,
                                                  delta_lambda=cdelt)
            
    if any([True
            for element in ['F_2KRON','F_2KRON_ERR'] if element in tabvalues]):
        print(table_filename+': k_scale = 2 ... '+get_timestring(starttime))

        f_2krons, f_2krons_err = \
            lsd_cat_measure_lib.fluxes_rkron_circ(mfs_cube,
                                                  x_1moms, y_1moms,
                                                  z_mins, z_maxs, r_krons,
                                                  k_scale=2.0,
                                                  err_cube=mfs_cube_err,
                                                  delta_lambda=cdelt)
            
    if any([True
            for element in ['F_3KRON','F_3KRON_ERR'] if element in tabvalues]):
          
        print(table_filename+': k_scale = 3 ...'+get_timestring(starttime))

        f_3krons, f_3krons_err = \
            lsd_cat_measure_lib.fluxes_rkron_circ(mfs_cube,
                                                  x_1moms, y_1moms,
                                                  z_mins, z_maxs, r_krons,
                                                  k_scale=3.0,
                                                  err_cube=mfs_cube_err,
                                                  delta_lambda=cdelt)

    if any([True
            for element in ['F_4KRON','F_4KRON_ERR'] if element in tabvalues]):
          
        print(table_filename+': k_scale = 4 ...'+get_timestring(starttime))

        f_4krons, f_4krons_err = \
            lsd_cat_measure_lib.fluxes_rkron_circ(mfs_cube,
                                                  x_1moms, y_1moms,
                                                  z_mins, z_maxs, r_krons,
                                                  k_scale=4.0,
                                                  err_cube=mfs_cube_err,
                                                  delta_lambda=cdelt)
        

# writing output fits table
var_list = []
unit_list = []
fits_format_list = []
for entry in tabvalues:
    # generate column vectors as requested by user and fill them
    entry_unit = out_line_form[entry][2]
    entry_format = out_line_form[entry][3]
    var_list.append(vars()[out_line_form[entry][1]])
    unit_list.append(entry_unit)
    fits_format_list.append(entry_format)

column_list = []
for tabvalue,var,unit,entry_format in zip(tabvalues,
                                          var_list,unit_list,
                                          fits_format_list):
    # make FITS columns
    column_list.append(Column(name=tabvalue,
                              unit=unit,
                              array=var,
                              format=entry_format)) 


# flange-mount to right side of input table
merged_fits_table_cols =  table.columns + fits.ColDefs(column_list) 
merged_fits_table = fits.BinTableHDU.from_columns(merged_fits_table_cols,
                                                  header=table_header)
merged_fits_table.writeto(outfitstable_name, overwrite=args.clobber)
print(table_filename+': DONE!!! Wrote FITS catalog '+outfitstable_name+' '+\
          get_timestring(starttime))


print(cats[4])   # ;-)
