#! /usr/bin/env python
#
# FILE:   lsd_cc_spatial.py
# AUTHOR: Christian Herenz
# DESCR.: Convolve all spectral slices with a gaussian filter, 
#         with FWHM given in arcseconds
#         with correct error propagation
#         uses pythons multiprocessing to split the work over 
#         several cores in a multicore machine

import lib.lsd_cat_lib as lsd_cat_lib
__version__ = lsd_cat_lib.get_version()

import sys,os,string,random,warnings,time
import argparse
import multiprocessing
import math as m
import pylab as p
import numpy as np
from astropy.io import fits
from scipy import signal
import gc

#  macOS since Sierra uses "spawn" 
multiprocessing.set_start_method('fork')

starttime = time.time()

######################################################################
# definitions of functions

# Memmap is now hard-coded False, as it leads to significant slow-down
# when being turned on
memmap = False

import lib.spatial_smooth_lib as spatial_smooth_lib  # spatial filtering functions
import lib.line_em_funcs as lef  # my own library with convenience functions
from lib.line_em_funcs import get_timestring
from lib.line_em_funcs import int_or_str

######################################################################
# command line parsing

# store command that was entered by the user
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command+' '+entity

# workaround to allow for negative numbers not being treated as command-line arguments
# https://stackoverflow.com/a/21446783/2275260
for i, arg in enumerate(sys.argv):
  if (arg[0] == '-') and arg[1].isdigit(): sys.argv[i] = ' ' + arg

# selectors
outSel = False
selMask = False

parser = argparse.ArgumentParser(description="""
Spatial cross correlation of all the layers in the datacube with a
wavelength dependent FSF kernel (approximated by a Moffat - can be
changed to Gaussian if requested) kernel. Operation is performed on 
signal and error is propagated accordingly.""",
                                 epilog="""
Note:  
Polynomials for the PSF FWHM and the beta parameter are in the form 
p(lam) = sum_n p_n (lam - lam0)^n, where lambda is in Angstrom and lam0 
is specified via --lambda0.  For the PSF FWHM the p_n's are defined via 
-pc and are assumed to be in units of arcsec/(Angstrom)^n - where n is the
order of the coefficient.   For the dimensionless beta parameter the p_n's 
are defined alike via -bc in units of 1/(Angstrom)^n.
""")
parser.add_argument("-i","--input",
                    required=True,
                    type=str,
                    help="""
                    Name of the input FITS file containing the flux (and variance) datacube.
                    """)
parser.add_argument("-o","--output",
                    type=str,
                    default=None,
                    help="""
                    Name of the output FITS file. The output FITS file will contain 2
                    HDUs: In HDU 0 the filtered signal is stored and HDU 1 contains the
                    propagated variances. [Default: `spatial_smoothed_+INPUT`,
                    i.e. `spatial_smoothed_` will be appended to the input file name.]
                    """)
parser.add_argument("-S","--SHDU",
                    type=int_or_str,
                    default='0',
                    help="""
                    HDU number (0-indexed) or name in the input FITS file containing the
                    flux data. [Default: 0]
                    """)
parser.add_argument("-N","--NHDU",
                    type=int_or_str,
                    default='1',
                    help="""
                    HDU number (0-indexed) or name in the input FITS file containing the
                    variance data. [Default: 1]
                    """)
parser.add_argument("--std",
                    action='store_true',
                    help="""
                    Some noise cubes are std not variance (e.g. in KMOS).  If set the
                    input noise cube will be squared to make it variance.
                    """)
parser.add_argument("--ignorenoise",
                    action='store_true',
                    help="""
                    Switch to not propagate the variance.  If set the output FITS file
                    will contain only 1 HDU that stores the filtered signal.
                    """)
parser.add_argument("-m","--mask",
                    type=str,
                    default=None,
                    help="""
                    Name of a FITS file containing a mask. [Default: none]
                    """)
parser.add_argument("-M","--MHDU",
                    type=int_or_str,
                    default='0',
                    help="""
                    HDU number (0-indexed) or name of the mask within MASK-file.  The
                    mask is supposed to be a 2D array with the same spatial dimensions
                    containing only ones and zeros.  Spaxels corresponding to zero-valued
                    pixels in the mask will be set to zero prior and post the spatial
                    convolution operation. [Default: 1]
                    """)
parser.add_argument("-P","--pixscale",
                    type=float,
                    default=0.2,
                    help="""
                    Size of a spaxel in arcseconds. [Default: 0.2]
                    """)
parser.add_argument("-t","--threads",
                    type=int,
                    default=multiprocessing.cpu_count(),
                    help="""
                    Number of CPU cores used in parallel operation. [Default: all available cpu cores]
                    """)
parser.add_argument("-bc", default=['3.5'], nargs='*',
                    help="""
                    List of polynomial coefficients (whitespace seperated) for the beta parameter of
                    the Moffat profile, i.e. -bc b0 b1 b2 .. bn. For m>n pm == 0.
                    (default: 3.5).
                    """)
parser.add_argument('-pc', default=['0.8'], nargs='*',
                    help="""
                    List of polynomial coefficients (whitespace seperated) for the PSF FWHM,
                    i.e. -pc p0 p1 ... pn. For m>n pm == 0.
                    (default: 0.8)
                    """)
parser.add_argument("--lambda0",
                    type=float,
                    default=7050,
                    help="""
                    Zero-point of the polynomial, in Angstrom.  [Default: 7050 Angstrom]
                    """)
parser.add_argument("--gaussian",
                    action='store_true',
                    help="""
                    Switch to use a Gaussian profile instead of the default Moffat
                    profile as spatial filter profile. The &beta; parameter will be
                    ignored in that case.
                    """)
parser.add_argument("-T","--truncconstant",
                    default=8,
                    type=float,
                    help="""
                    Parameter controlling the truncation of the filter window: the filter
                    is truncated at T*WIDTH-PARAM - were WIDTH-PARAM = sigma for Gaussian
                    and FWHM for Moffat. [Default: 8]
                    """)


args = parser.parse_args()

# fixed trunc_constant - defines where filter windows is truncated
# filter_window == 0 <=> r > trunc_constant*width_parameter + 1
# width_parameter = sigma for Gaussian - FWHM for Moffat
# - with this choice the Moffat filter is ~ twice as large as the Gaussian
# at same trunc-constant
trunc_constant = args.truncconstant

input_filename = args.input
data_hdu = args.SHDU
stat_hdu = args.NHDU

mask_hdu = args.MHDU

if args.mask != None:
    selMask = True
    mask_filename = args.mask
else:
    mask_filename = 'no_mask'
if args.output == None:
    out_filename = 'spatial_smoothed_'+input_filename
else:
    out_filename = args.output

use_gaussian = args.gaussian

pix_scale = args.pixscale
num_threads = args.threads

std = args.std
ignorenoise = args.ignorenoise

# polynomial coefficients for FWHM and beta
# (reversed, for np.polyval)
pc = [float(pcn) for pcn in args.pc][::-1]   
bc = [float(bcn) for bcn in args.bc][::-1]  
lambda_0 = args.lambda0

######################################################################
# program starts here

# welcome message:
program_name =  __file__.split('/')[-1]
filter_name = 'Gaussian' if use_gaussian else 'Moffat'
print(program_name+' version '+str(__version__))
print(program_name+' run on datacubes in inputfile: '+input_filename+\
          ' (flux in HDU: '+str(data_hdu)+', variance in HDU: '+\
          str(stat_hdu)+')')
if selMask == False:
    print(program_name+': Using no mask cube!')
else:
    print(program_name+': Using mask cube in '+mask_filename+' (HDU: '+\
              str(mask_hdu)+')')
print(program_name+': PSF shape model = '+filter_name)
print(program_name+': PSF FHWM(lambda) dependence via polynomial with coefficients ' + \
      ', '.join(['p'+str(i[0])+'='+str(i[1]) for i in enumerate(pc[::-1])]))
if filter_name == 'Moffat':
    print(program_name+': Moffat beta(lambda) dependence via polynomial with coefficients ' + \
      ', '.join(['b'+str(i[0])+'='+str(i[1]) for i in enumerate(bc[::-1])]))
print(program_name+': Reference wavelength lambda0 in PSF polynomial(s): ' + str(lambda_0) + \
      ' Angstrom')
print(program_name+': plate scale = '+str(pix_scale)+' arcsec')
print(program_name+': Using ' +str(num_threads) + ' parallel threads')
print(program_name+': Spatial covolution using Fast Fourier transform!')

# reading in the flux data & header
print(input_filename+': Reading in the Data Cube... (HDU'+str(data_hdu)+') '+\
          get_timestring(starttime))
cube_data,cube_header = lef.read_hdu(input_filename,
                                     data_hdu,
                                     nans_to_value=False,
                                     memmap=memmap)  #first  we keep the nans, but
# we set all NaNs to zero here, otherwise the FFT filtering produces rubbish
nancube = np.isnan(cube_data)   # this nan-selector cube is also used
                                # in the end to set all original nans
                                # to zero (so FFT artifacts in the border regions are ignored)
cube_data[nancube] = 0  # note: with newer astropy > 3, convolve(...,nan_treatment='fill')
                        # might do the same (TODO: check)

crval3 = cube_header['CRVAL3']
try:
    cdelt3 = cube_header['CD3_3']
except KeyError:
    cdelt3 = cube_header['CDELT3']
crpix3 = cube_header['CRPIX3']
try:
    cunit3 = cube_header['CUNIT3']
except KeyError:
    cunit3 = 'Angstrom'
    print('WARNING: No CUNIT3 Keyword specifying the wavelength unit found. Assuming Angstrom.')

if selMask == True:
    print(str(input_filename)+': Applying mask '+str(mask_filename)+\
              ' (HDU'+str(mask_hdu)+') to Data (File '+str(input_filename)+\
              ', HDU '+str(data_hdu)+')... '+get_timestring(starttime))
    mask,mask_head = lef.read_hdu(mask_filename,
                                  mask_hdu,
                                  memmap=memmap)
    cube_data *= mask
else:
    mask = [] # just a dummy
    print('No mask is given... Performing all operations on umasked cube.\
 (File '+input_filename+', HDU '+str(data_hdu)+')... '+\
              get_timestring(starttime))

print(input_filename+': Threaded Filtering starts...'+\
          get_timestring(starttime))
print(input_filename+' ... Filter window PSFs are '+\
          filter_name+'s ...') # hope this is always the right plural ;-)

##################### ACTUAL COMPUTATION STARTS HERE ####################

# convolving all sclices (split over the processors) of data 
# with wavelength dependent gaussian kernel:

# 1. MAKE WAVELENGTH DEPENDENT FILTER SHAPES
# ------------------------------------------
length = cube_data.shape[0]
print(input_filename+\
          ': Creating the wavelength dependent PSF filter for '+\
          str(length)+' datacube layers. '+\
          get_timestring(starttime))

# xax is the array containing the wavelength values in Angstrom! the
# unit is important here - since a polynomial can be used to describe
# the wavelength dependence of the PSFs FWHM - in this case the
# coefficents describe a polynomial FWHM(lambda[Angstrom])
xax = crval3+cdelt3*(p.arange(length)-crpix3)  # Angstrom - default in MUSE
if cunit3 == 'nm':
    xax *= 10  # e.g. early MUSE cubes
elif cunit3 == 'um':
    xax *= 1E4  #  KMOS pipeline default
elif cunit3 != 'Angstrom':
    print('ERROR: Unknown wavelength unit: '+cunit3)
    sys.exit(2)

if use_gaussian:
    filter_windows = spatial_smooth_lib.makeGaussians_poly(
        xax=(xax - lambda_0), p=pc, pix_scale=pix_scale,
        trunc_constant=trunc_constant)
else:
    filter_windows = spatial_smooth_lib.makeMoffats_poly(
        xax=(xax - lambda_0), p=pc, b=bc, pix_scale=pix_scale,
        trunc_constant=trunc_constant)

print(input_filename+\
          ": Average size of the filter windows  "+\
          str(filter_windows[int(len(filter_windows)/2)].shape[0])+\
          "^2 px. "+\
          get_timestring(starttime))

# create also the filters for the error propagation (below)
filter_windows_squared = [np.square(filter_window)
                          for filter_window in filter_windows]

# 2. ITERATATE OVER THE SLICES AND FILTER THEM 
# --------------------------------------------
# (split up over several processors)
filtered = spatial_smooth_lib.filter_parallel(filter_windows,
                                              cube_data,
                                              num_threads,
                                              selMask,
                                              mask,
                                              filename=input_filename,
                                              method='fft')
filtered[nancube] = 0  # setting all nans in the original cube to zero in the output datacube 

mask = [] # delete old mask, and dummy it

# header
cube_header['EXTNAME'] = 'DATA_2DSMOOTH'
cube_header['CCS'] = (os.path.basename(sys.argv[0]),
                      'spatial cross-correlation (CCS) routine')
cube_header['CCSV'] = (__version__,
                       'CCS version')
cube_header['CCSC'] = (command,
                       'CCS full command')
# cube_header['CCSM'] = (method,
#                        'CCS method - normal or fft')
cube_header['CCSIN'] = (input_filename,
                        'CCS input filename')
cube_header['CCSINS'] = (data_hdu,
                         'CCS input data HDU - 0-indexed')
cube_header['CCSINN'] = (stat_hdu,
                         'CCS input variance HDU - 0-indexed')
cube_header['CCSPXSC'] = (pix_scale,
                          'assumed pix scale arcsec/pix in datacube')
cube_header['CCSMSK'] = (mask_filename,
                         'SSC mask filename')
cube_header['CCSMSKH'] = (mask_hdu,
                          'CCS mask filename HDU')
cube_header['CCSFILT'] = (filter_name,
                          'CCS filter funtion - i.e. Moffat or Gaussian')
cube_header['CCSFILTT'] = (trunc_constant,
                           'CCS filter truncated at CCSFILTT x FWHM')
cube_header['CCSPLY'] = (True,
                         'p(l) = sum_n p_n (l-l0)^n')
for pi in enumerate(pc[::-1]):
    if pi[0] == 0:
        unit = '[arcsec]'
    elif pi[0] == 1:
        unit = '[arcsec/AA]'
    else:
        unit = '[arcsec/AA**'+str(pi[0])+']'

    cube_header['CCSPLYP'+str(pi[0])] = (pi[1],
                                         ' '.join(['p_'+str(pi[0]), unit]))

if filter_name == 'Moffat':
    cube_header['CCSBETA'] = (True,
                              'beta(l) = sum_n b_n (l-l0)^n')
    for bi in enumerate(bc[::-1]):
        if bi[0] == 0:
            unit = '[1]'
        elif bi[0] == 1:
            unit = '[1/AA]'
        else:
            unit = '[1/AA**'+str(bi[0])+']'

        cube_header['CCSB'+str(bi[0])] = (bi[1],
                                          ' '.join(['b_'+str(bi[0]), unit]))
        
cube_header['CCSPLYL0'] = (lambda_0,
                           'l0')


# treating the variances
if not ignorenoise:
    # writing out temporary FITS file, since we do not want to store 2
    # cubes (data and variance) in memory
    # genrating random 6 digit or letter input_filename for temporary
    # fits file (e.g.  KNX9RC):
    tempfilename = ''.join(random.choice(string.ascii_uppercase + string.digits) 
                           for x in range(6))
    tempfilename = tempfilename+'.fits'
    print(input_filename+': Writing out temporary filtered cube '+tempfilename+' ... '+\
              get_timestring(starttime))
    lef.write_primary(filtered,cube_header,tempfilename)
    del filtered
    del cube_data
    gc.collect()

    # NOW DOING THE SAME FOR THE NOISE, BUT WITH SQUARED MATRIX & WITHOUT MASK
    # (assuming noise is stored as variance)
    print(input_filename+': Reading in the Noise Cube... (HDU'+str(stat_hdu)+') '+\
              get_timestring(starttime))
    stat_data,stat_head = lef.read_hdu(input_filename,
                                       stat_hdu,
                                       nans_to_value=True,
                                       memmap=memmap)

    if std:
        # If the input noise cube is std (KMOS default), we need to square it
        stat_data = stat_data ** 2.

    stat_head['EXTNAME'] = 'STAT_2DSMOOTH'
    for key in cube_header:
        if 'CCS' in key:
            stat_head[key] = cube_header[key]
    print(input_filename+': Error propagation in the noise cube... '+\
              get_timestring(starttime))

    # error propagation on all sclices of stat for the filtering
    filtered_stat = spatial_smooth_lib.filter_parallel(filter_windows_squared,
                                                       stat_data,
                                                       num_threads,
                                                       selMask,
                                                       mask,
                                                       filename=input_filename,
                                                       method='fft')

    print(input_filename+': Writing out final data & deletion of temporary files... '+\
              get_timestring(starttime))
    filtered,filtered_header = lef.read_hdu(tempfilename,0,memmap=memmap)
    filtered_data_hdu = fits.PrimaryHDU(data=filtered,header=filtered_header)
    os.remove(tempfilename)
    del filtered ; del filtered_header
    filtered_noise_hdu = fits.ImageHDU(data=filtered_stat,header=stat_head)
    out_hdu_list = fits.HDUList(hdus=[filtered_data_hdu,filtered_noise_hdu])
    out_hdu_list.writeto(out_filename,
                         overwrite=True,
                         output_verify='silentfix')
    print(input_filename+\
          ': All Done! Written spatial smoothed data & propagated error to '+\
          out_filename+' . '+\
          get_timestring(starttime))
else:
    print(input_filename+': Writing out cross-correlated flux... '+\
              get_timestring(starttime))
    cube_header['EXTNAME'] = 'DATA_2DSMOOTH'
    lef.write_primary(filtered,cube_header,out_filename)
    print(input_filename+\
          ': All Done! Written spatial smoothed data '+\
              out_filename+' . '+\
              get_timestring(starttime))
    
