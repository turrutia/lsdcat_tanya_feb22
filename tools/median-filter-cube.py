#! /usr/bin/env python
#
# FILE:    median-filter-cube.py
# AUTHOR:  E. C. Herenz
# DATE:    June 2012 / May 2020
# DESCR.:  Apply a median filter in spectral direction to the MUSE Datacube 
#          and subtract the filter from the cube.
#          (do not alter the variances though, i.e. median filter is only applied
#          to flux datacube)

# python standard library
import sys
import os
import argparse
import multiprocessing
import time
# numpy / scipy / astropy
import numpy as np
from scipy.ndimage import filters
from astropy.io import fits
# lsdcat/lib/line_em_funcs.py
import line_em_funcs as lef
import lsd_cat_lib
from median_filter_lib import notch_interpol, medfilt_cube_para

#  macOS since Sierra uses "spawn"
try:
    multiprocessing.set_start_method('fork')
except RuntimeError:
    pass

__version__ = lsd_cat_lib.get_version()

# get string of the commandline that was entered by the user
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command+' '+entity

starttime = time.time()
get_timestring = lef.get_timestring

# parse command line
parser = argparse.ArgumentParser(
    description="""
    Subtract an in spectral direction
    median-filtered version of the datacube.  Can be used to remove sources that have
    significant detectable continuum signal within the datacube.
    """)
parser.add_argument("fitscube",
                    type=str,
                    help="""
                    The FITS File that contains the flux- and variances datacube
                    """)
parser.add_argument("-S","--signalHDU",default=1,type=int,
                    help="""
                    HDU number (0-indexed) containing the flux in fitscube  
                    (default: 1).
                    """)
parser.add_argument("-V","--varHDU",default=2,type=int,
                    help="""
                    HDU number (0-indexed) containing the variances in fitscube
                    (default: 2).  Variances will just be copied over to the output cube.
                    Use --varHDU=-1 if this is not desired or if there is no variance cube.
                    """)
parser.add_argument("-o","--output",default=None,
                    help="""
                    Name of the output FITS file. 
                    (default: median_filtered_W+<width>+<fitscube>)
                    """)
parser.add_argument("-W","--width",default=151,type=int,
                    help="""
                    The median filter width in spectral pixels.  If an even number is
                    given, the actual filter value will be this number +1. (Default: 151)
                    """)
parser.add_argument("--ao",action='store_true',
                    help=""" 
                    Fill notch-filter blocked region with linear interpolated values.
                    This prevents an debiases the median filter subtracted
                    datacube close to the edges of the notch filter.  See also the
                    parameters --notch_start, --notch_finish, and --notch_bound.
                    """)
parser.add_argument("--notch_start", type=float, default=5803.,
                    help="""
                    Start of the notch-filter blocked wavelength range (in Angstrom, default: 5803).
                    """)
parser.add_argument("--notch_end", type=float, default=5970.,
                    help="""
                    End of the notch-filter blocked wavelength range (in Angstrom, default: 5970).
                    """)
parser.add_argument("--notch_bound", type=int, default=30, 
                    help=""" Size of window before- and after notch-filter blocked region to determine base points
                    for the interpolation.  The base point is calculated as the median over this window.
                    """)
parser.add_argument("-t","--num_cpu",default=None,type=int,
                    help="""
                    Number of CPU cores to be used (default: all available).
                    """)
parser.add_argument("--memmap",action='store_true',
                    help=""" 
                    Use memory mapping. 
                    Reduces memory usage, but might also reduce speed of execution.
                    """)
args = parser.parse_args()

# number of threads, either defined by user or use all CPUs
num_threads =  args.num_cpu
if num_threads == None:
    num_threads = multiprocessing.cpu_count()

width = args.width
if width % 2 == 0:
    # filter width must be uneven
    width = width + 1

fitscube = args.fitscube
signalHDU = args.signalHDU  # hdu indices are passed to the programm in 0-indexed
varHDU = args.varHDU        # notation
output = args.output

memmap = True if args.memmap else False

if output == None:
    # default output file name
    output = 'median_filtered_W'+str(width)+'_'+fitscube

print(fitscube+': Reading input flux (HDU'+str(signalHDU)+')... '+\
          get_timestring(starttime))
fitscube_hdu_list = fits.open(fitscube)
data, header = lef.read_hdu(fitscube,signalHDU, memmap=memmap)


# actual operations 
if args.ao:
    xax = lef.wavel(header)
    notch_start_pix = np.argwhere(xax <= args.notch_start)[-1]
    notch_end_pix = np.argwhere(xax >= args.notch_end)[0]
    print(str(fitscube)+': Linear interpolating over notch filter from '+\
          str(args.notch_start)+' Angstrom (z_pix='+str(notch_start_pix)+') to '+\
          str(args.notch_end)+' Angstrom (z_pix='+str(notch_end_pix)+'.')
    print(str(fitscube)+':  Width before & after filter to calculate median  '+\
          str(args.notch_bound)+' (spectral bins).')

    medfiltered_data = medfilt_cube_para(data, width, num_threads=num_threads,
                                         ao=True,
                                         notch_start_px=notch_start_pix,
                                         notch_end_px=notch_end_pix,
                                         notch_bound=args.notch_bound,
                                         verbose=True,
                                         fitscube=fitscube,starttime=starttime)
else:
    medfiltered_data = medfilt_cube_para(data, width, num_threads=num_threads,
                                         verbose=True,
                                         fitscube=fitscube, starttime=starttime)
    
med_sub_data = data - medfiltered_data

# prepare for writing output to disk
header['EXTNAME'] = 'MEDFILTERED_DATA'
header['MFR'] = (os.path.basename(sys.argv[0]),
                 'median filter subtract routine')
header['MFRC'] = (command,
                  'MFR full command called')
header['MFRV'] = (__version__,
                  'MFR version')
header['MFRW'] = (width,
                  'MFR median filter width')
header['MFRIN'] = (fitscube,
                   'ENR input FITS file')
header['MFRINS'] = (signalHDU,
                    'ENRIN flux HDU number')
if varHDU >= 0:
    header['MFRINN'] = (varHDU,
                        'ENRIN variance HDU number')
    
if args.ao:
    header['MFRAO'] = (1, 'notch-gap interpolated')
    header['AO_S'] = (args.notch_start, '[Angstrom] notch gap start')
    header['AO_E'] = (args.notch_end, ' [Angstrom] notch gap end')
    header['AO_W'] = (args.notch_bound, ' width of notch interp. (pixel)')

med_sub_header = header.copy()
med_sub_header['EXTNAME']  ='MEDFILTER-SUBTRACTED_DATA'

# write final output for object detection
med_sub_data_hdu = fits.PrimaryHDU(data = med_sub_data,
                                   header = med_sub_header)
medfiltered_data_hdu = fits.ImageHDU(data = medfiltered_data,
                                     header = header)
if varHDU >= 0:
    variance, varhead = lef.read_hdu(fitscube, varHDU, memmap=memmap)
    variance_hdu = fits.ImageHDU(data = variance,
                             header = varhead)
    hdu_list = [med_sub_data_hdu, variance_hdu, medfiltered_data_hdu]
else:
    hdu_list = [med_sub_data_hdu, medfiltered_data_hdu]
    
# input cube contains exposure count HDU
try:
    ex_hdu = fitscube_hdu_list['EXP']
    hdu_list.append(ex_hdu)
except:
    pass

out_hdu_list = fits.HDUList(hdus=hdu_list)
out_hdu_list.writeto(str(output),overwrite=True,output_verify='silentfix')

print(str(fitscube)+
      ': All done! Filtered datacube (with unchanged variances) stored in: '+
      str(output)+
      ' (HDU 1: Original minus Medianf., HDU2: Variances, HDU3: Medianf.) - '+
      get_timestring(starttime))
