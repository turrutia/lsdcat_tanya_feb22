#! /usr/bin/env python
from __future__ import absolute_import

# FILE: gen_noise_spec.py
# DATE: 01/2015
# AUTHORS: C. HERENZ & J KERUT
# DESCR.: 
# Creates a spectrum from a number of randomly scattered spectra 
# that are in regions where no bright objects are close. Can be
# used as an estimate of the real noise in the cube. Does not need 
# a mask as it figures out the positions from a catalogue which 
# must contain SExtractor information.

__version__ = '1.0.2'



import gc
import sys
import os
import argparse
import math as m
from random import choice
import random

# numpy / matplotlib
import numpy as np
import matplotlib.pyplot as plt
import pylab as p
from scipy.special import erfinv

# Astropy
from astropy import wcs as astWCS
from astropy.io import fits
from astropy.io import ascii

from time import time

# own libraries
import line_em_funcs as lef
from gen_noise_spec_lib import pix_to_radec, radec_to_pix, spec_in_ap

# get string of the commandline that was entered by the user
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command+' '+entity


parser = argparse.ArgumentParser(description="""
Creates a spectrum from a number of randomly scattered spectra that
are in regions where no bright objects are close. Can be used as an
estimate of the real noise in the cube. Does not need a mask as it
figures out the positions from a catalogue which must contain
SExtractor information.
""")
parser.add_argument('input_cube',type=str,
                    help="""
The input MUSE cube filename.
""")
parser.add_argument('input_cat',type=str, 
                    help="""
The input catalog (SExtractor Format). Needs to have columns RA, DEC,
FLUX, A_IMAGE, B_IMAGE, THETA, KRON_RADIUS (pix). The name of the 
column FLUX is specified using the --fluxstring option.
""")  # TODO: allow for different flux measures
parser.add_argument('--pixsize',type=float,default=0.06,
                    help="""
Pixelsize of image from which the catalog was derived. (Default: 0.06)
""")
parser.add_argument('--limmag',type=float,default=25.,help="""
Include only objects that are brighter than this magnitude in the mask.
(Default: 25))
""")
parser.add_argument('--fluxunit',type=str,default='uJy',help="""
Fluxunit in the catalog: uJy (default) or mag. 
""")
parser.add_argument('--catalogfields',type=str,
                    default='RA,DEC,ACS_F814W_FLUX,A_IMAGE,B_IMAGE,THETA_IMAGE,KRON_RADIUS',
                    help="""
A comma-separated list of the fieldnames that are required from the catalog.
(Default: 'RA,DEC,ACS_F814W_FLUX,A_IMAGE,B_IMAGE,THETA_IMAGE,KRON_RADIUS').
                    """)
parser.add_argument('--number',type=int,default=100,help="""
Number of random spectra used to generate the noise spectrum. 
(Default: 100)
""")
parser.add_argument('--maxiter',type=int,default=300,help="""
Maximum number of iterations for placement of the apertures. (Default: 300)
""")
parser.add_argument('--radius', type=float, default=5.,help="""
Radius of extraction aperture (in MUSE pixel). (Default: 5 pixel)
""")
parser.add_argument('--hdudat',type=int, default=1, help="""
Index of HDU containing the data.  0-indexed. Default = 1.
""")
parser.add_argument('--hduerr',type=int,default=2, help="""
Index of HDU containing the variance. 0-indexed. Default = 2.
""")
parser.add_argument('--whitelightfile',type=str,default='none',
                    help="""FITS filename, where white light image is
                    stored.  (default: none - i.e. use input_cube)""")
parser.add_argument('--hduwhite',type=int, default=4,
                    help="""Index of HDU containing the white light 
                    image. Used to exclude regions without data,
                    i.e.  spaxels with NaN in the white light image.
                    0-indexed. Default = 4.
                    """)
parser.add_argument('--timesbigger',type=float, default=1., help="""
Scale factor for the size of the ellipses. (Default: 1)
""")
parser.add_argument('--showplots',action='store_true',help="""
Show diagnostic plots at the end, to asses whether the process produced a
meaningful result.
""")
parser.add_argument('--outfilename',type=str,default='none',help="""
Filename of output FITS file. (Default: input_cube[-.fits]_1derrspec.fits)
""")
parser.add_argument('--clobber',action='store_true', help="""
Overwrite existing FITS files.
""")
parser.add_argument('--savefigs',action='store_true', help="""
Save diagnostic plots.
""")

args = parser.parse_args()

start1 = time()

####################################################################

muse_sampling = 0.2  # arcseconds (FIX)
cat_pix_sampling = args.pixsize
input_cat = args.input_cat
input_cube = args.input_cube
print('gen_noise_spec.py - Working on datacube '+input_cube+\
          ' - using catalog '+input_cat)

# include objects that are brighter than this magnitude in mask
lim_mag = args.limmag   # note: the current catalog (Guo+ 2013)
                        # contains flux in uJy
print('Including only objects brighter than '+str(lim_mag)+' magnitude in catalog' )
# make mask bigger
times_bigger = args.timesbigger 

# number of spectra 
#number = 100  
number = args.number

# radius of extracted spectra (in pixel)
#radius = 5. 
radius = args.radius

max_iter = args.maxiter

####################################################################

############### READ FILES AND CUBES ###############################

### read catalogue with SExtractor information
assert input_cat[-3:] == 'cat' or input_cat[-4:] == 'fits'
if input_cat[-3:] == 'cat':
    table = ascii.read(input_cat,format='sextractor')
else:
    table_hdu = fits.open(input_cat)
    table = table_hdu[1].data
catalogfields = args.catalogfields
catalogfields_list = catalogfields.split(',')
cat_RA = table[catalogfields_list[0]]
cat_DEC = table[catalogfields_list[1]]
cat_FLUX = table[catalogfields_list[2]]
cat_A_IMAGE = table[catalogfields_list[3]]
cat_B_IMAGE = table[catalogfields_list[4]]
cat_THETA_IMAGE = table[catalogfields_list[5]]
cat_KRON_RADIUS = table[catalogfields_list[6]]

assert args.fluxunit == 'uJy' or args.fluxunit == 'mag'
# we work in uJy internally
# convert limiting magnitude to limiting flux
lim_flux = (10**(lim_mag/(-5./2.))*3631)*10**(6)
if args.fluxunit == 'mag':
    cat_FLUX = (10**(cat_FLUX/(-5./2.))*3631)*10**(6)

### read MUSE data cube file
input_file_muse = input_cube
hdu_muse = fits.open(input_file_muse,
                     memmap=False)  
header_muse   = hdu_muse[args.hdudat].header
if not 'CD3_3' in header_muse:
    cdel_key = 'CDELT3'
else:
    cdel_key = 'CD3_3'   # if this does not work, than something is
                         # wrong

crpix = header_muse['CRPIX3']
crval = header_muse['CRVAL3']
cdelt = header_muse[cdel_key]
    

if args.whitelightfile == 'none':
    muse_white    = hdu_muse[args.hduwhite].data
    muse_white_header = hdu_muse[args.hduwhite].header
else:
    muse_white_hdu = fits.open(args.whitelightfile)
    muse_white = muse_white_hdu[args.hduwhite].data
    muse_white_header = muse_white_hdu[args.hduwhite].header
    
data_muse = hdu_muse[args.hdudat].data

# compute wavelength
# wavelength = np.arange(start_muse,step_muse*len(data_muse)+start_muse,
#                        step_muse)
wavelength = lef.wavel(header_muse, naxis=3, cdel_key=cdel_key)

nan_sel = np.isnan(data_muse)

############## SORT OUT RELEVANT OBJECTS ###########################

# cube dimensions
xmax, ymax, zmax = data_muse.shape[2], data_muse.shape[1], data_muse.shape[0]

# (spatial) pixel grid
ygrid, xgrid = p.indices((ymax,xmax),float)

MUSE_matrix = np.zeros((ymax,xmax))

# Include a margin of 10 MUSE-pixels to select objects, that partially
# overlap with the field of view
edge_coordinates = pix_to_radec([0 - 10, xmax + 10],
                                [0 - 10, ymax + 10],
                                header_muse)

# find index of all objects that are brighter than specified above
# with lim_flux
index_bright = np.asarray([i for i in range(len(cat_FLUX)) if
                           cat_FLUX[i] > lim_flux])


# find index of all objects in the field of view
index_fov_RA  = [i for i in range(len(cat_RA)) 
                 if edge_coordinates[0][1] < cat_RA[i] < edge_coordinates[0][0]]
index_fov_DEC = [i for i in range(len(cat_DEC)) 
                 if edge_coordinates[1][0] < cat_DEC[i] < edge_coordinates[1][1]]

index_fov_RA_DEC = np.asarray([i for i in range(len(index_fov_RA)) 
                               if index_fov_RA[i] in index_fov_DEC])

index_fov = np.asarray(index_fov_RA)[index_fov_RA_DEC]

# merge both specifications
index_fov_bright = np.asarray([i for i in range(len(index_fov))
                               if index_fov[i] in index_bright])
index =  index_fov[index_fov_bright]

####################################################################

### now find x and y positions inside ellipses

masked = []
mask_x = []
mask_y = []

start = time()

# xgrid_hst,ygrid_hst = p.indices((x_range,y_range),float)


for a in range(len(index)):

    A2 = cat_A_IMAGE[index][a] * cat_KRON_RADIUS[index][a]
    B2 = cat_B_IMAGE[index][a] * cat_KRON_RADIUS[index][a]
    A2 *= cat_pix_sampling / muse_sampling
    B2 *= cat_pix_sampling / muse_sampling

    Theta = cat_THETA_IMAGE[index][a]

    pos_ra = cat_RA[index][a]
    pos_dec = cat_DEC[index][a]
    x_y_pos = radec_to_pix([pos_ra],[pos_dec],header_muse)

    x_pos = x_y_pos[0]
    y_pos = x_y_pos[1]

    dy = ygrid - y_pos
    dx = xgrid - x_pos
    
    radius_all = p.sqrt(((np.cos(m.radians(-Theta))*(dx)-
                          np.sin(m.radians(-Theta))*(dy))/A2)**2 + 
                        ((np.sin(m.radians(-Theta))*(dx)+
                          np.cos(m.radians(-Theta))*(dy))/B2)**2)

    select  = radius_all <=  float(times_bigger)

    MUSE_matrix[select] = 1

stop = time()

print("Time for creating mask = "+str(round(stop-start,2))+'s')

nans = p.isnan(muse_white)
MUSE_matrix[nans] = 1


start = time()
print('Finding random positions and extracting spectra ...')

# create random position and check if it is far enough from mask
# stops if search failed 3 times the number of spectra required

MUSE_matrix_selector = MUSE_matrix.copy()
app_matrix = np.zeros_like(MUSE_matrix)
list_random_pos = []
list_apertures = []
list_random_spectra = []
list_random_variance_spectra = []
count = 0
i=0
while len(list_random_spectra) < number and count < max_iter:
    x_random = random.randint(radius, xmax - radius)
    y_random = random.randint(radius, ymax - radius)

    xgrid2 = xgrid - x_random
    ygrid2 = ygrid - y_random

    xygrid_circle = np.sqrt(xgrid2**2+ygrid2**2)
    select = xygrid_circle <= radius 
    select2 = xygrid_circle > radius 
    xygrid_circle[select] = 1
    xygrid_circle[select2] = 0

    diff = xygrid_circle - MUSE_matrix_selector

    summe = np.sum(diff[select])
    
    if len(list_random_spectra) == 0 and count == 0:
        sum_app = np.sum(xygrid_circle)
        # random_error = error_muse[:,select].sum(axis=1)

    if summe == sum_app and np.sum(nan_sel[:,select]) == 0:
        random_spectrum = data_muse[:,select].sum(axis=1)
        list_random_spectra.append(random_spectrum)
        list_random_pos.append([x_random,y_random])
        list_apertures.append(select)

        MUSE_matrix_selector[select] = 1  # we don't want overlapping
                                          # apertures.
        app_matrix[select] = 1


    else:
        count+=1
        
if len(list_random_spectra)<number:
    print("Within the maximum number of itereations ("+\
          str(max_iter)+") "+\
          "I was not able to place "+str(number)+" of apertures. "+\
          "Aborting now. Try to change parameters!")
    sys.exit(2)
else:
    print("Randomly placed "+str(number)+" apertures.  "+\
          "(In random placement loop "+str(count)+\
          " where discarded.)")
    
stop = time()
print("Time for finding random positions and extracting spectra = "+\
      str(round(stop-start,2))+'s')
      
print("Extracting variance spectra from cube ...")
start = time()
# freeing some memory
del data_muse
gc.collect()

variance_muse = hdu_muse[args.hduerr].data
variance_spectrum_sum = np.zeros(variance_muse.shape[0])
for random_aperture in list_apertures:
    variance_spectrum = variance_muse[:,select].sum(axis=1) / np.sum(select)
    list_random_variance_spectra.append(variance_spectrum)
    variance_spectrum_sum += variance_spectrum

mean_variance = variance_spectrum_sum / len(list_apertures)

stop = time()
print('Time for extracting variance spectra from cube '+\
      str(round(stop-start,2))+'s')
          


####################################################################

print('Computing quartiles...')

z_len = zmax

quartile1_list = []
quartile2_list = []
equiv_gauss_sigma = []
mean_error = []

quartile1 = np.percentile(np.asarray(list_random_spectra), 25,
                          axis=0)
quartile2 = np.percentile(np.asarray(list_random_spectra), 75, 
                          axis=0)
# converting into variance, see e.g. Eq. 3.49 in Statistics, Data
# Mining and Machine Learning in Astronomy
#equiv_gauss_var = ( erf(0.5) * m.sqrt(2)  * (quartile1 - quartile2) )**2
equiv_gauss_var =  ( (quartile1 - quartile2) / (2 * m.sqrt(2) * erfinv(0.5)) )**2
equiv_gauss_var /= np.pi*radius**2 # per pixel


####################################################################

stop1 = time()
#print xmax, ymax

if args.outfilename == 'none':
    outfilename = input_file_muse.replace('.fits',
                                          '_1derrspec.fits')
else:
    outfilename = args.outfilename
print('Saving output: '+outfilename)



# figure showing the  error spectrum
shrt = 12.
lng = 14.
fig1 = plt.figure(num=1,figsize=(lng,shrt/2.))
fig1.canvas.set_window_title('Derived Sigma-Spectrum ')
ax = fig1.add_subplot(211)

ax.plot(wavelength,
        np.sqrt(equiv_gauss_var),
        color='black', label = r'$\sigma_\mathrm{effective}$')
ax.plot(wavelength,
        np.sqrt(mean_variance),
        color='grey', label=r'$\sigma_\mathrm{propagated}$')

ax.legend()

if header_muse['BUNIT'] == '10**(-20)*erg/s/cm**2/Angstrom':
    unit_string = '$10^{-20}\mathrm{erg}\,\mathrm{s}^{-1}'+\
                  '\mathrm{cm}^{-2}\mathrm{\AA{}}^{-1}$'
else:
    unit_string = 'cgs'
ax.set_ylabel('$\sigma_\lambda$ ['+unit_string+']')

ax2 = fig1.add_subplot(212)
ax2.plot(wavelength,
         np.sqrt(equiv_gauss_var/mean_variance),
         color='black')
ax2.hlines(1,wavelength[0],wavelength[-1],linestyles='--')
ax2.set_ylabel(r'$\sigma_\mathrm{effective} / \sigma_'+\
               '\mathrm{propagated}$')

ax2.set_xlabel('Wavelength ['+header_muse['CUNIT3']+']')
ax.set_xlim((wavelength[0],wavelength[-1]))
ax2.set_xlim((wavelength[0],wavelength[-1]))

# figure showing a map of the apertures
fig2 = plt.figure(num=2, figsize=(8,8))
fig2.canvas.set_window_title('Map of Apertures + Catalog Objects')
ax = fig2.add_subplot(111)
ax.set_title('Map of Apertures')
ax.imshow(app_matrix - MUSE_matrix)

if args.showplots:
    plt.show()
if args.savefigs:
    fig1.savefig(outfilename[:-5]+'_errspeccomp.pdf')
    fig2.savefig(outfilename[:-5]+'_errspecapps.pdf')

# HDU 1 - effective variance spectrum
header_1 = fits.Header()
header_1['CRPIX1'] = crpix
header_1['CRVAL1'] = crval
header_1['CD1_1'] = cdelt
header_1['BUNIT'] = '('+header_muse['BUNIT']+')**2'
header_1['ENR'] = (os.path.basename(sys.argv[0]),
                    'effective noise generation routine')
header_1['ENRV'] = (__version__,
                    'ENR version')
header_1['ENRC'] = (command,
                    'ENR full command called')
header_1['ENRIN'] = (input_cube,
                     'ENR input FITS file')
header_1['ENRINS'] = (args.hdudat,
                      'ENRIN flux HDU number')
header_1['ENRINN'] = (args.hduerr,
                      'ENRIN variance HDU number')
header_1['ENRCAT'] = (input_cat,
                      'ENR catalog for object mask')
header_1['ENRCATS'] = (cat_pix_sampling,
                       'ENRCAT pix sampling')
header_1['ENRCATLM'] = (lim_mag,
                       'ENRCAT limiting magnitude')
header_1['ENRCATF'] = (args.fluxunit,
                       'ENRCAT flux unit')
header_1['ENRCATFF'] = (catalogfields,
                        'ENRCAT catalog fields')
header_1['ENRNUM'] = (number,
                      'ENR number of apertures')
header_1['ENRRAD'] = (radius,
                      'ENR radius of apertures')
header_1_orig = header_1.copy()
header_1.add_comment('1D EFFECTIVE NOISE')

data_1 = equiv_gauss_var

# HDU 2 - average (within apertures) pipeline propagated variance spectrum
data_2 = mean_variance
header_2 = header_1_orig.copy()
header_2['EXTNAME'] = 'PPVAR_AVG'
header_2['BUNIT'] = '('+header_muse['BUNIT']+')**2'
header_2.add_comment('AVERAGE PIPELINE PROPAGATED VARIANCE IN'+\
                     ' APERTURES')


# HDU 3 - position of apertures & regions, which were avoided
header_3 = fits.Header()
header_3 = header_muse[header_muse.index('CRPIX1'):
                       header_muse.index('CRVAL2')+1]
header_3['EXTNAME'] = 'APMAP'
header_3.add_comment('POSITION OF APERTURES AND REGIONS, WHICH'+\
                     ' WERE AVOIDED')
data_3 = app_matrix - MUSE_matrix

# HDU4 - row stack of the individual spectra
header_4 = header_1_orig.copy()
header_4['EXTNAME'] = 'RSS_SPEC'
header_4['BUNIT'] = header_muse['BUNIT']
header_4.add_comment('ROW STACKED RANDOM SPECTRA')
array_random_spectra = np.asarray(list_random_spectra)

# HDU5 - row stack of the variance spectra in the apertures
header_5 = header_1_orig.copy()
header_5['EXTNAME'] = 'VAR_RSS_SPEC'
header_5['BUNIT'] = '('+header_muse['BUNIT']+')**2'
header_5.add_comment('ROW STACKED APERTURE AVERAGED VARIANCE'+\
                     ' SPECTRA')
array_avg_variance_spectra = np.asarray(list_random_variance_spectra)

# HDU 6 - TABLE OF ALL POSITIONS
array_random_pos = np.asarray(list_random_pos)
table_col1 = fits.Column(name='x_pos',format='I',
                         array=array_random_pos[:,0])
table_col2 = fits.Column(name='y_pos',format='I',
                         array=array_random_pos[:,1])
cols = fits.ColDefs([table_col1,table_col2])
table_hdu = fits.BinTableHDU.from_columns(cols)
table_hdu.header['EXTNAME'] = 'TABLE'
table_hdu.header['RADIUS'] = (radius,'AP RADIUS IN PIXEL')
table_hdu.header.add_comment('TABLE OF POSITION APERTURES')

# write out the whole fits file
hdu_list = fits.HDUList([fits.PrimaryHDU(
                             data=data_1,
                             header=header_1),
                         fits.ImageHDU(
                             data=data_2,
                             header=header_2),
                         fits.ImageHDU(
                             data=data_3,
                             header=header_3),
                         fits.ImageHDU(
                             data=array_random_spectra,
                             header=header_4),
                         fits.ImageHDU(
                             data=array_avg_variance_spectra,
                             header=header_5),
                         table_hdu])
hdu_list.writeto(outfilename, overwrite=args.clobber)


print('gen_noise_spec.py done! :) Total time = '+str(stop1-start1)+\
      's')

















