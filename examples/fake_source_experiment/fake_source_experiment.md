# Fake Source Datacubes #

The fake source datacubes can be downloaded from <http://ttt.astro.su.se/~ehere/lsdcat_test_cubes/>.

These datacubes are based on a median-filter subtracted (151 spectral
pixel in width) version of publicly available MUSE Hubble Deep Field
South datacube: <http://data.muse-vlt.eu/HDFS/v1.20/IMAGE-HDFS-v1.24.fits.gz>

The median filter subtraction utilised the script
`median-filter-cube.py` in LSDCat's `/tools/` folder with the following
command:

	median-filter-cube.py DATACUBE-HDFS-v1.24.fits
	
Additionally we applied the effective noise that we used an empirical
noise calibration according to the prescriptions given in Sect. 4.3 of
the LSDCat Paper. This was done with the script `apply_eff_noise.py`
from LSDCat's tool folder:

	apply_eff_noise.py median_filtered_W151_DATACUBE-HDFS-v1.24.fits DATACUBE-HDFS-v1.24_1derspec.fits --NHDU=1 --blowup
	
The file `DATACUBE-HDFS-v1.24_1derspec.fits` used in the above command
contains our empirical calculated noise for each spectral layer, and
it is also contained in this folder.

The cubes are cut in wavelength range from 4970 to 5028.75 Angstrom,
and the artificial sources are inserted at 5000 Angstrom.

The naming scheme is `HDFS_wPS_wl5000_flux-FLUXLEVEL_Posgrid51.fits`,
where `FLUXLEVEL` is the logarithm of the total flux in erg/s/cm^2 of
the artificially implanted emission line sources (from `-16.2` to
`-18.5`).


# Fake Source Position Table #

The table `51_fake_source_positions_for_hdfs.fits` in this directory
contains the positions of the inserted emission lines.  The columns
are: `id`, `x_fake`, and `y_fake`.


# Pre-Procssing before Matched Filtering #

In the fake source insertion and recovery experiment we avoid the
edges and the region around the brightest star in the field, as there
the background noise properties are considerably different with
respect to the rest of the datacube.

- Edges: `xmin:xmax = 12:312`, `ymin:ymax = 14:318`
- Star:  `xmin:xmax = 90:130`, `ymin:ymax = 60:105`
  
  
The scripts `mask_star.py` and `mask_edges.py` in this directory
replace the datacube values in above regions with zero.

The following commands apply the scripts to all fake source datacubes:


	for file in HDFS*fits
	do 
		python ../mask_star.py $file 0
	done

	for file in masked_star_HDFS_wPS_wl5000_flux-1*
	do
		python ../mask_edges.py $file 0
	done
	
	
# Running LSDCat #

On the output of the above commands we now perform the matched
filtering step of `LSDCat`.  The polynomial coefficients
`-p0=0.78296921` and `-p1=-4.214e-5` were determined from a Gaussian
fit to the brightest star in the HDFS datacube.

## Spatial Filtering ##

	for file in edge_masked_masked_star_HDFS_wPS_wl5000_flux-1*fits
     do 
         lsd_cc_spatial.py --input=${file} --SHDU=0 --NHDU=1 --gaussian -p0=0.78296921  -p1=-4.214e-5
     done

## Spectral Filtering ##

	for file in spatial_smoothed_edge_masked_masked_star_HDFS_wPS_wl5000_flux-1*fits
	do
        lsd_cc_spectral.py -i ${file} --FWHM 250
	done

## Signal to noise cubes ##


	for file in wavelength_smooth_spatial_smoothed_edge_masked_masked_star_HDFS_wPS_wl5000_flux-1*
	do
         s2n-cube.py -i $file -o sn_${file}
	done
	
## LSDCat - intermediate catalogue ##

	for file in sn*Posgrid51*.fits
	do
          lsd_cat.py -i $file -t 8
	done


## LSDCat - final catalogue including measurements ##

	for fl in 16.{2..9} 17.{0..9} 18.{0..5}
	do
		lsd_cat_measure.py \
		-ic catalogue_sn_wavelength_smooth_spatial_smoothed_edge_masked_masked_star_HDFS_wPS_wl5000_flux-${fl}_Posgrid51.fits.fits \
		-ta 4 \
		-f HDFS_wPS_wl5000_flux-${fl}_Posgrid51.fits \
		-ff wavelength_smooth_spatial_smoothed_edge_masked_masked_star_HDFS_wPS_wl5000_flux-${fl}_Posgrid51.fits;
	done

This last command produces 24 catalogues:

`catalogue_sn_wavelength_smooth_spatial_smoothed_edge_masked_masked_star_HDFS_wPS_wl5000_flux-${FLUX}_Posgrid51.fits_fluxes.fits`

Here `${FLUX}` contains all logarithmic flux values from 16.2 to 18.5.

These catalogues are then used to verify the software as described in
Sect. 3 of the LSDCat paper.  Note that the catalogues contain 5 real
emission lines that are not part of the fake source insertion
experiment!
