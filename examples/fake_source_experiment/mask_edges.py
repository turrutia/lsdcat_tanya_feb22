from astropy.io import fits
import numpy as np
import sys

filename = sys.argv[1]
hdu_num = sys.argv[2]
print('Masking edges for '+sys.argv[1])

print('reading data...')
hdu = fits.open(filename,memmap=False)
data = hdu[int(hdu_num)].data

print('selecting mask...')
select = np.ones_like(data[0,:,:])
select[14:318,12:312] = 0
select = select.astype('bool')
print('masking...')
data[:,select] = 0.

print('writing out...')
hdu.writeto('edge_masked_'+filename)
print('edge_masked_'+filename)
print('done!')

